package com.edreams.koinpoc.account

import java.util.*

interface AccountRepositoryInterface {
    fun getAccountData(): String
    fun getAccountName(): String
}

class AccountRepository(val userName: String) : AccountRepositoryInterface {
    private var data: String = "No init"

    init {
        data = Date().time.toString()
    }


    override fun getAccountData() = data
    override fun getAccountName() = userName
}


