package com.edreams.koinpoc.account

import org.koin.dsl.module


val accountModule = module {
    scope<AccountActivity> {
        scoped { (name: String) -> AccountRepository(name) }
    }
}