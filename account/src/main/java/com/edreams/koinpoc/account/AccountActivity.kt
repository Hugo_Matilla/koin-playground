package com.edreams.koinpoc.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.core.data.RepositoryInterface
import kotlinx.android.synthetic.main.activity_account.*
import org.koin.android.ext.android.inject
import org.koin.android.scope.lifecycleScope
import org.koin.core.KoinComponent
import org.koin.core.parameter.parametersOf

class AccountActivity : AppCompatActivity(), KoinComponent {

    private val repo: AccountRepository by lifecycleScope.inject { parametersOf("John") }
    private val repoCore: RepositoryInterface by inject()

    companion object {
        fun start(caller: Activity) {
            caller.startActivity(Intent(caller, this::class.java.declaringClass))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        val name = repo.getAccountName()
        val data = repo.getAccountData()

        repo.getAccountData().also { println("🚛 $it") }
        repo.getAccountData().also { println("🚛 $it") }

        accountText.text =
            "ACCOUNT: \n\nInjected Parameter: \n$name \n\nScoped Repo:\n$data \n\nSingleton Repo:\n${repoCore.getCreationTime()}"

    }
}
