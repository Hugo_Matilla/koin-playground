package com.edreams.koinpoc.detail

import android.content.Context
import com.edreams.koinpoc.R
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*

class DetailPresenter(private val context: Context) : KoinComponent {

    private val randomEmojiGenerator: RandomEmojiGenerator by inject()

    private val date: Date = Date()

    fun getText() = "${getRandomEmoji()} \n${getTitle()}"

    fun getRandomEmoji() = randomEmojiGenerator.emoji

    private fun getTitle() =
        "\ncontext.getString(R.string.app_name): " + context.getString(R.string.app_name)

}
