package com.edreams.koinpoc.detail


class RandomEmojiGenerator() {
    val emoji = "FACTORY: " + listOf("😀", "🚛", "🏄", "🚀", "📱").random()
}
