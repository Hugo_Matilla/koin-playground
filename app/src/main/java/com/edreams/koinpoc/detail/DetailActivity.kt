package com.edreams.koinpoc.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.edreams.koinpoc.R
import com.edreams.koinpoc.di.detailModule
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.ext.android.inject
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class DetailActivity : AppCompatActivity() {

    private val presenter: DetailPresenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        loadKoinModules(detailModule)
        detailTextView.text = presenter.getText()
    }

    override fun onDestroy() {
        super.onDestroy()
        unloadKoinModules(detailModule)
    }

    companion object {
        fun start(caller: Activity) {
            caller.startActivity(Intent(caller, this::class.java.declaringClass))
        }
    }
}
