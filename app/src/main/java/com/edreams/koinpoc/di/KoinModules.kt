package com.edreams.koinpoc.di

import com.edreams.koinpoc.detail.DetailPresenter
import com.edreams.koinpoc.detail.RandomEmojiGenerator
import com.edreams.koinpoc.home.HomePresenter
import org.koin.dsl.module

val mainModule = module {
    factory { HomePresenter(get()) }
}

val detailModule = module {
    factory { RandomEmojiGenerator() }
    factory { DetailPresenter(get()) }
}