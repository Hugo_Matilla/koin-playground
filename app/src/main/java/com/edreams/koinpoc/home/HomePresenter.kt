package com.edreams.koinpoc.home

import com.example.core.data.RepositoryInterface
import org.koin.core.KoinComponent

class HomePresenter(private val repo: RepositoryInterface) : KoinComponent {

    fun getText() = repo.getData()
}

