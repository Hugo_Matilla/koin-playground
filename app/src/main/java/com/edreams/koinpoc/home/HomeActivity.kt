package com.edreams.koinpoc.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.edreams.koinpoc.R
import com.edreams.koinpoc.account.AccountActivity
import com.edreams.koinpoc.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity() {

    private val presenter: HomePresenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        getDataButton.setOnClickListener {
            textView.text = presenter.getText()
        }

        goToDetailButton.setOnClickListener {
            DetailActivity.start(this)
        }

        goToAccountButton.setOnClickListener {
            AccountActivity.start(this)
        }
    }
}
