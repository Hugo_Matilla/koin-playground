package com.edreams.koinpoc

import android.app.Application
import com.edreams.koinpoc.account.accountModule
import com.edreams.koinpoc.di.mainModule
import com.example.core.di.coreModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class KoinPlaygroundApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@KoinPlaygroundApplication)
            androidLogger()
            modules(
                coreModule,
                mainModule,
//                detailModule,
                accountModule
            )
            println("🚛 Koin Ended")
        }
    }
}