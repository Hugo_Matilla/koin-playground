package com.edreams.koinpoc

import com.edreams.koinpoc.di.mainModule
import com.edreams.koinpoc.home.HomePresenter
import com.example.core.data.RepositoryInterface
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.get
import org.koin.test.inject
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.BDDMockito.given
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
val mainModuleTest = module {
    single<RepositoryInterface> { FakeRepository() }
    factory { HomePresenter(get()) }
}

class FakeRepository : RepositoryInterface {
    override fun getCreationTime() = "Fake Today"
    override fun getData() = "Fake Data"
}

class KoinUnitTest : KoinTest {

    @Before
    fun setup() {
        startKoin { modules(mainModuleTest) }

    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun myTest() {
        val presenter: HomePresenter = get()
        val res = presenter.getText()
        assertEquals("Fake Data", res)
    }
}


class KoinUnitTest2 : KoinTest {

    private val presenter: HomePresenter by inject()
    private val repo: RepositoryInterface by inject()

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(mainModule)
    }

    @Test
    fun myTest2() {
        declareMock<RepositoryInterface>()

        given(repo.getData()).will { "Mock Data" }
        val res = presenter.getText()
        assertEquals("Mock Data", res)
        verify(repo, times(1)).getData()
    }
}



