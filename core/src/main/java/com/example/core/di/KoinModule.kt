package com.example.core.di

import com.example.core.data.Repository
import com.example.core.data.RepositoryInterface
import org.koin.dsl.module

val coreModule = module {
    single<RepositoryInterface> { Repository() }
}