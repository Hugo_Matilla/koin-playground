package com.example.core.data

import java.util.*

interface RepositoryInterface {
    fun getData(): String
    fun getCreationTime(): String
}

class Repository : RepositoryInterface {

    private val date: Date = Date()

    override fun getCreationTime() = date.time.toString()

    override fun getData() = getRepoCreationTime()

    private fun getRepoCreationTime() = "SINGLETON: \nRepo creation\ntime: \n${date.time}"


}
